package pl.dzienniczekmedyczny.siatkacentylowa;

import java.io.IOException;
import java.util.List;
import pl.dzienniczekmedyczny.db.CentyleSQLiteHelper;
import pl.dzienniczekmedyczny.db.Centyle05DataSource;
import pl.dzienniczekmedyczny.db.entry.CentyleEntry;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class SiatkaCentylowaActivity extends Activity {

	private Centyle05DataSource centyleds;
	private CentyleSQLiteHelper database;
	private List<CentyleEntry> listCentyle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_siatka_centylowa);
		
		/**
		 * Sprawdzenie czy baza istnieje,
		 * je�li nie to kopiuje j� z assets�w
		 * @author Marcin
		 */
		database = new CentyleSQLiteHelper(this);
		try {
			database.createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		database.close();
		
		
		centyleds = new Centyle05DataSource(this);
		centyleds.open();
		listCentyle = centyleds.getEntryByAge(CentyleSQLiteHelper.TABLE_G_WEIGHT_05, 150);
		CentyleEntry entry = listCentyle.get(0);
		Log.i("m4",""+entry);
		listCentyle = centyleds.getEntryInTime(CentyleSQLiteHelper.TABLE_G_WEIGHT_05, 150, 3);
		Log.i("m4",""+listCentyle);
	}
	/**
	 * otwiera/zamyka po��czenie z baz� w razie pauzy/zamkni�cia activity
	 * @author Marcin
	 */
	@Override
	protected void onPause() {
		centyleds.close();
		super.onPause();
		}
	/**
	 * 
	 */
	@Override
	protected void onResume(){
		centyleds.open();
		super.onResume();
	}
}
