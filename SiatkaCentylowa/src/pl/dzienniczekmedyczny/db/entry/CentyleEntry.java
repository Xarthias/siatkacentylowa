package pl.dzienniczekmedyczny.db.entry;

public class CentyleEntry {
	private int age;
	private float L;
	private float M;
	private float S;
	private float p01;
	private float p1;
	private float p3;
	private float p5;
	private float p10;
	private float p15;
	private float p25;
	private float p50;
	private float p75;
	private float p85;
	private float p90;
	private float p95;
	private float p97;
	private float p99;
	private float p999;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public float getL() {
		return L;
	}
	public void setL(float l) {
		L = l;
	}
	public float getM() {
		return M;
	}
	public void setM(float m) {
		M = m;
	}
	public float getS() {
		return S;
	}
	public void setS(float s) {
		S = s;
	}
	public float getP01() {
		return p01;
	}
	public void setP01(float p01) {
		this.p01 = p01;
	}
	public float getP1() {
		return p1;
	}
	public void setP1(float p1) {
		this.p1 = p1;
	}
	public float getP3() {
		return p3;
	}
	public void setP3(float p3) {
		this.p3 = p3;
	}
	public float getP5() {
		return p5;
	}
	public void setP5(float p5) {
		this.p5 = p5;
	}
	public float getP10() {
		return p10;
	}
	public void setP10(float p10) {
		this.p10 = p10;
	}
	public float getP15() {
		return p15;
	}
	public void setP15(float p15) {
		this.p15 = p15;
	}
	public float getP25() {
		return p25;
	}
	public void setP25(float p25) {
		this.p25 = p25;
	}
	public float getP50() {
		return p50;
	}
	public void setP50(float p50) {
		this.p50 = p50;
	}
	public float getP75() {
		return p75;
	}
	public void setP75(float p75) {
		this.p75 = p75;
	}
	public float getP85() {
		return p85;
	}
	public void setP85(float p85) {
		this.p85 = p85;
	}
	public float getP90() {
		return p90;
	}
	public void setP90(float p90) {
		this.p90 = p90;
	}
	public float getP95() {
		return p95;
	}
	public void setP95(float p95) {
		this.p95 = p95;
	}
	public float getP97() {
		return p97;
	}
	public void setP97(float p97) {
		this.p97 = p97;
	}
	public float getP99() {
		return p99;
	}
	public void setP99(float p99) {
		this.p99 = p99;
	}
	public float getP999() {
		return p999;
	}
	public void setP999(float p999) {
		this.p999 = p999;
	}
	@Override
	public String toString() {
		return "CentyleEntry [age=" + age + ", L=" + L + ", M=" + M + ", S="
				+ S + ", p01=" + p01 + ", p1=" + p1 + ", p3=" + p3 + ", p5="
				+ p5 + ", p10=" + p10 + ", p15=" + p15 + ", p25=" + p25
				+ ", p50=" + p50 + ", p75=" + p75 + ", p85=" + p85 + ", p90="
				+ p90 + ", p95=" + p95 + ", p97=" + p97 + ", p99=" + p99
				+ ", p999=" + p999 + "]";
	}
	

}
