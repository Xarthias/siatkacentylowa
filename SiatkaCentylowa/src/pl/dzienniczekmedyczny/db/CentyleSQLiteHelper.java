package pl.dzienniczekmedyczny.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;

public class CentyleSQLiteHelper extends SQLiteOpenHelper {
	
	private static String DB_PATH;
	private static final String DATABASE_NAME = "siatkacentylowa.db";
	private static final int DATABASE_VERSION = 2;
	//v1 - tabele girlweight05 i girllength05
	//v2 - dodano tabele boyweight05 i boylength05
	
	private Context context;
	static SQLiteDatabase database;

	//nazwy tabel z percentylami
	public static final String TABLE_G_WEIGHT_05 = "girlweight05";
	public static final String TABLE_G_LENGHT_05 = "girllength05";
	public static final String TABLE_B_WEIGHT_05 = "boyweight05";
	public static final String TABLE_B_LENGHT_05 = "boylength05";
	
	//Nazwy kolumn dla tabel percentylowych
	public static final String COLUMN_AGE = "age";
	public static final String COLUMN_L = "L";
	public static final String COLUMN_M = "M";
	public static final String COLUMN_S = "S";
	public static final String COLUMN_P01 = "p01";
	public static final String COLUMN_P1 = "p1";
	public static final String COLUMN_P3 = "p3";
	public static final String COLUMN_P5 = "p5";
	public static final String COLUMN_P10 = "p10";
	public static final String COLUMN_P15 = "p15";
	public static final String COLUMN_P25 = "p25";
	public static final String COLUMN_P50 = "p50";
	public static final String COLUMN_P75 = "p75";
	public static final String COLUMN_P85 = "p85";
	public static final String COLUMN_P90 = "p90";
	public static final String COLUMN_P95 = "p95";
	public static final String COLUMN_P97 = "p97";
	public static final String COLUMN_P99 = "p99";
	public static final String COLUMN_P999 = "p999";
	
	//String z nazwami kolumn
	public static final String[] allColumnsPercetile = {
			COLUMN_AGE, COLUMN_L, COLUMN_M, COLUMN_S,
			COLUMN_P01,	COLUMN_P1, COLUMN_P3,
			COLUMN_P5, COLUMN_P10, COLUMN_P15,
			COLUMN_P25,	COLUMN_P50,	COLUMN_P75,
			COLUMN_P85,	COLUMN_P90,	COLUMN_P95,
			COLUMN_P97,	COLUMN_P99,	COLUMN_P999
			};
	public static String[] getAllPercentileColumnNames() {
		return allColumnsPercetile;
		}
	
/**	private static final String PercentileColumns = 
			COLUMN_AGE + " integer, "
			+ COLUMN_L + " real, "
			+ COLUMN_M + " real, "
			+ COLUMN_S + " real, "
			+ COLUMN_P01 + " real, "
			+ COLUMN_P1 + " real, "
			+ COLUMN_P3 + " real, "
			+ COLUMN_P5 + " real, "
			+ COLUMN_P10 + " real, "
			+ COLUMN_P15 + " real, "
			+ COLUMN_P25 + " real, "
			+ COLUMN_P50 + " real, "
			+ COLUMN_P75 + " real, "
			+ COLUMN_P85 + " real, "
			+ COLUMN_P90 + " real, "
			+ COLUMN_P95 + " real, "
			+ COLUMN_P97 + " real, "
			+ COLUMN_P99 + " real, "
			+ COLUMN_P999 + " real"; */
	
	public CentyleSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
	// TODO Auto-generated method stub
		//database.close();
		context.deleteDatabase(DATABASE_NAME);
		try {
			this.createDataBase();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		DB_PATH = context.getApplicationInfo().dataDir.toString() + "/databases/";
//		Log.i("m4",DB_PATH);
	}
	
	//sprawdzenie czy baza istnieje
	public void createDataBase() throws IOException{
		boolean databaseExist = checkDataBase();
		if(databaseExist){
			// nic nie r�b
			}
		else {
			this.getWritableDatabase();
			copyDataBase();
			}
		}
	
	public boolean checkDataBase() {
		File databaseFile = new File(DB_PATH + DATABASE_NAME);
		return databaseFile.exists(); 
		}
	
	private void copyDataBase() throws IOException {
		//Otwiera plik bazy jako input stream
		InputStream myInput = context.getAssets().open(DATABASE_NAME);
		// �cie�ka do bazy danych
		String outFileName = DB_PATH + DATABASE_NAME;
		//otwiera pust� baz� jako output stream
		OutputStream myOutput = new FileOutputStream(outFileName);
		//transferuje bity z inputu do outputu
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
			}
		//zamykamy streamy
		myOutput.flush();
		myOutput.close();
		myInput.close();
		}
		
		/**
		* Metoda zamykaj�ca po��czenie z baz�.
		*/
		@Override
		public synchronized void close() {
			if(database != null)
				database.close();
			super.close();
			}
}
