package pl.dzienniczekmedyczny.db;

import pl.dzienniczekmedyczny.db.entry.CentyleEntry;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

public class Centyle05DataSource {
	
	private SQLiteDatabase database;
	private CentyleSQLiteHelper dbHelper;
	
	public Centyle05DataSource(Context context) {
		dbHelper = new CentyleSQLiteHelper(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	/**
	 * przepisywanie kursora do entry
	 * @param cursor
	 * @return entry
	 * @author Marcin
	 */
	private CentyleEntry cursorToEntry(Cursor cursor){
		CentyleEntry entry = new CentyleEntry();
		entry.setAge(cursor.getInt(0));
		entry.setL(cursor.getFloat(1));
		entry.setM(cursor.getFloat(2));
		entry.setS(cursor.getFloat(3));
		entry.setP01(cursor.getFloat(4));
		entry.setP1(cursor.getFloat(5));
		entry.setP3(cursor.getFloat(6));
		entry.setP5(cursor.getFloat(7));
		entry.setP10(cursor.getFloat(8));
		entry.setP15(cursor.getFloat(9));
		entry.setP25(cursor.getFloat(10));
		entry.setP50(cursor.getFloat(11));
		entry.setP75(cursor.getFloat(12));
		entry.setP85(cursor.getFloat(13));
		entry.setP90(cursor.getFloat(14));
		entry.setP95(cursor.getFloat(15));
		entry.setP97(cursor.getFloat(16));
		entry.setP99(cursor.getFloat(17));
		entry.setP999(cursor.getFloat(18));	
		return entry;
	}
	
	/**
	 * przepisywanie kursora do entry
	 * @param cursor
	 * @return entry
	 * @author Marcin
	 */
	private CentyleEntry cursorToSmallEntry(Cursor cursor){
		CentyleEntry entry = new CentyleEntry();
		entry.setAge(cursor.getInt(0));
		entry.setP3(cursor.getFloat(6));
		entry.setP25(cursor.getFloat(10));
		entry.setP50(cursor.getFloat(11));
		entry.setP75(cursor.getFloat(12));
		entry.setP97(cursor.getFloat(16));	
		return entry;
	}
	
	public List<CentyleEntry> getEntryByAge(String table, int age) {
		List<CentyleEntry> entries = new ArrayList<CentyleEntry>();
		Cursor cursor = database.query(
				table, 
				CentyleSQLiteHelper.getAllPercentileColumnNames(), 
				CentyleSQLiteHelper.COLUMN_AGE + "=" + age,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			CentyleEntry entry = cursorToEntry(cursor);
			entries.add(entry);
			cursor.moveToNext();
			}
		cursor.close();
		return entries;
	}
	
	public List<CentyleEntry> getEntryInTime(String table, int age, int interval) {
		List<CentyleEntry> entries = new ArrayList<CentyleEntry>();
		Cursor cursor = database.query(
				table, 
				CentyleSQLiteHelper.getAllPercentileColumnNames(), 
				CentyleSQLiteHelper.COLUMN_AGE + ">=" + (age-interval)
				+ " AND " + CentyleSQLiteHelper.COLUMN_AGE + "<=" + (age+interval),
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			CentyleEntry entry = cursorToSmallEntry(cursor);
			entries.add(entry);
			cursor.moveToNext();
			}
		cursor.close();
		return entries;
	}
	
}
